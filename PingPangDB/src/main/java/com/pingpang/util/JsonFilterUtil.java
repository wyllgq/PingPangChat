package com.pingpang.util;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.pingpang.websocketchat.ChartUser;

public final class JsonFilterUtil {

    public static void addFilterForMapper(ObjectMapper mapper) {
    	
    	Set<String> sets =new HashSet<String>();
    	sets.add("userEmail");
    	sets.add("userPhone");
    	sets.add("userImagePath");
    	sets.add("userPassword");
    	sets.add("userSex");
    	sets.add("userStatus");
    	sets.add("liveType");
    	sets.add("ip");
    	sets.add("userCreateDate");
        SimpleBeanPropertyFilter fieldFilter = SimpleBeanPropertyFilter.serializeAllExcept(sets);
        SimpleFilterProvider filterProvider = new SimpleFilterProvider().addFilter("fieldFilter", fieldFilter);
        mapper.setFilterProvider(filterProvider).addMixIn(ChartUser.class, FieldFilterMixIn.class);
    }

    /**
     * 定义一个类或接口
     */
    @JsonFilter("fieldFilter")
    interface FieldFilterMixIn{
    }
}
