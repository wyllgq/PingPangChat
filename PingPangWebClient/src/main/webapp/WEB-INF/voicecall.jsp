<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>视频通话测试</title>
    <script src="${httpServletRequest.getContextPath()}/jquery.min.js"></script>
    <script src="${httpServletRequest.getContextPath()}/audio/HZRecorder.js"></script>
    <script src="${httpServletRequest.getContextPath()}/audio/voiceEncoder.js"></script>
</head>

<body name="voice">
    <table>
        <tr style = "display : none">
            <td>
                <label for="txtSelfId">当前用户:</label>
                <input type="text" id="txtSelfId" class="w120" readonly="readonly"/>
            </td>
            <td align="right">
                <label for="txtTargetId">目标用户:</label>
                <input type="text" id="txtTargetId" class="w120" readonly="readonly"/>
            </td>
        </tr>
        <tr>
            <td>
               <audio id="audio_from" controls autoplay></audio>
            </td>
            <td>
               <audio id="audio_accept" controls autoplay></audio>
             </td>
        </tr>
        <tr>
            <td align="center"><span id="fromUser"></span></td>
            <td align="center"><span id="remoteUser"></span></td>
        </tr>
        <tr><!-- 
            <td align="center"><button name="button" onclick="star();">录制</button></td>
            <td align="center"><button name="button" onclick="stop();">暂停</button></td>
            <td align="center"><button name="button" onclick="paly();">播放</button></td>
            <td align="center"><button name="button" onclick="sendVoice();">传送</button></td> -->
        </tr>
    </table>

    <script type="text/javascript">
    var recorder;
    var audio;
    
    //var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    //var source;
    
	function star(){
    audio=document.querySelector('#audio_from');
	//console();
	$('#audio_from').attr('src',"");
	HZRecorder.get(function (rec) {
        recorder = rec;
        recorder.start();
    }, {
            sampleBits: 8,
            sampleRate: 16000
        });
	
	//source = audioCtx.createBufferSource();
	}
	
	function stop(){
		recorder.stop();
	}
	
	
   function paly(){
	recorder.play(audio);
	}
   
	 var blobChunk;
     var CHUNK_SIZE = 1024*2;
     var start = 0;
     var end = CHUNK_SIZE;
	function sendVoice(){
		//$('#audio_from').attr('src',"");
		//recorder.stop();
		
		 /*  var voiceBlob=recorder.getBlob();
		 blobChunk = new Blob([]);
		 var SIZE = voiceBlob.size;
	     while (start < SIZE) {
	    	 blobChunk=voiceBlob.slice(start, end);
	         start = end;
	         end = start + CHUNK_SIZE;
	         console.log("start:"+start+" end:"+end);
	         blobToDataURI(blobChunk,function(result){    //blob格式再转换为base64格式
		     //document.getElementById('img2').src = result;
		     result="data:audio/wav;"+result.substring(result.indexOf("base64"));
		     console.log(result);
			 parent.sendMsg($("#txtTargetId").val(),14,result);
	         });
	    } */
	    
	    //console.log("ARRAY:"+recorder.getPcm());
		//const json = JSON.stringify(recorder.get32Array());
		//console.log("ARRAY:"+json);
	    
	    //var voiceBlob=recorder.getBlobNotStop();
	    /*console.log("send");
	    var voiceBlob=recorder.getPcm();
	    blobToDataURI(voiceBlob,function(result){    //blob格式再转换为base64格式
		     //document.getElementById('img2').src = result;
		     console.log("base64:"+result);
			 parent.sendMsg($("#txtTargetId").val(),14,result);
	     });
	     */
	     
	     var voiceBase64=recorder.getPcm();
	     recorder.clear();
	     if(null==voiceBase64 || ""==voiceBase64){
	    	 return;
	     }
	     //var zipBase64=zip(voiceBase64);
	     //console.log("zip:"+zipBase64);

	     parent.sendMsg($("#txtTargetId").val(),14,voiceBase64);
	    //recorder.clear();
		//recorder.audioData.size=0;
		//recorder.audioData.buff=[];
		//recorder.start();
	}
	var i=1;
	function remotePlay(blobVoice){
		console.log(i++);
		

		//var remoteAudio=document.getElementById('#audio_accept');
		//remoteAudio.src = window.URL.createObjectURL(dataURItoBlob(blobVoice));
		//remoteAudio.play();
		//$("#audio_accept").attr("src",window.URL.createObjectURL(dataURItoBlob(blobVoice)));
	    //$("#audio_accept").attr("preload", 'auto');
	    //recorder.clear();
	    
		/* audioCtx.decodeAudioData(_base64ToArrayBuffer(blobVoice), function(buffer) {
        source.buffer = buffer;

        source.connect(audioCtx.destination);
        source.loop = true;
       }); */
       console.log("unZip1:"+blobVoice.length);
       //var unZipBase64=unzip(blobVoice);
       //console.log("unZip2:"+unZipBase64);
       //var buffer =_base64ToArrayBuffer(unZipBase64);
	   var buffer=stringToUint8Array(blobVoice);
       var audioContext = new ( window.AudioContext || window.webkitAudioContext )();
	   var fileResult =addWavHeader(buffer, '16000', '8', '1');//解析数据转码wav
			audioContext.decodeAudioData(fileResult, function(buffer) {
			   _visualize(audioContext,buffer);//播放
			});
       
		//$("#audio_accept").get('0').load();
	}
	
	star();
	setInterval(sendVoice ,800);
	
	function test1(){
		//alert("test1");
	}
	

	function stringToUint8Array(str){
		  var arr = [];
		  for (var i = 0, j = str.length; i < j; ++i) {
		    arr.push(str.charCodeAt(i));
		  }
		 
		  var tmpUint8Array = new Uint8Array(arr);
		  return tmpUint8Array
     }    
	</script>
</body>

</html>